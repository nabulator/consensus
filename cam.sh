#!/bin/bash
# rkll.sh
# -------------------------
# Let's kill all python processes

hosts=( 192.168.2.2 
	192.168.2.4 
	192.168.2.5 
	192.168.2.6 
	192.168.2.7 
	192.168.2.8 
	192.168.2.10 
	192.168.2.11)
#files=(cloud.py KSVD.py)
for i in "${!hosts[@]}"; do 
	set -x
		ssh "${hosts[i]}" /bin/bash <<- EOSSH
				pkill -f raspivid
				nohup raspivid -o "vid-$i.h264" -t 30000 > foo.out 2> foo.err < /dev/null & 
			EOSSH
	{ set +x; } 2>/dev/null
done
