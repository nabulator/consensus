#!/bin/bash
# grab.sh
# -------------------------
# Automate grabbing data from nodes

#hosts=(green cyan white magenta)
hosts=(192.168.2.2 
192.168.2.4 
192.168.2.5 
192.168.2.6 
192.168.2.7 
192.168.2.8 
192.168.2.10 
192.168.2.11)
files=("vid-*.h264")

for i in "${!hosts[@]}"; do 
	for j in "${!files[@]}"; do
		scp  "${hosts[i]}":"${files[j]}" ./pickles
	done
done
