from socket import *
from array import *
import time
import threading
import logging
import sys
import argparse
import signal
import json
import random as rd

ADDR = '192.168.1.255'
PORT = 4490
HOST = 0
LENGTH = 2
BROADCAST_COUNT = 10
DISGUSTING = 5

# setup argparse
parser = argparse.ArgumentParser(description='Aynschronous Consensual Node',
        epilog='')
parser.add_argument('-i', dest='host_id', help='Manually Assigns ID to node', type=int)
parser.add_argument('-n', dest='count', type=int, default=2, 
        help='Number of elements in array to transmit', )
parser.add_argument('-b', dest='broadcast_count', type=int, default=10, 
        help='Number of times this node broadcasts')
parser.add_argument('-s', dest='std_deviation', type=float, default=1.0,
        help='Standard Deviation of random generator')
parser.add_argument('-N', dest='nodes_up', type=int, default=4,
        help='Number of total nodes up and running. Used to help estimate when\
        to terminate program. Default: 4') 

args = parser.parse_args()

# restricts host range to one byte
if args.host_id:
    HOST = args.host_id % 256
else:
    HOST = rd.randint(0,255)
LENGTH = args.count
BROADCAST_COUNT = args.broadcast_count 

# setup logging
logging.addLevelName(5, 'DISGUSTING')
FORMAT = '%(asctime)s [%(levelname)-4.4s] {' + str(HOST) + '} %(message)s'
logging.basicConfig(format=FORMAT, level=logging.WARNING)

log = logging.getLogger()
handler = logging.FileHandler('formated_' + str(HOST) + '.log')
handler.setLevel(logging.INFO)
handler.setFormatter( logging.Formatter('%(message)s') )
log.addHandler(handler)

logging.info('Host ID: %i', HOST)
logging.info('Standard Deviation %f', args.std_deviation)


# stores history in JSON
history = []
f = open('history_' + str(HOST) + '_' + str(args.std_deviation) + '.json', 'w')

def save_file():
    global f
    global history
    f.write( json.dumps( history ) )
    f.close()

# setup sig handler
terminate_now = False
def exit_graceful(signum, frame):
    print('ute Puppies')
    global terminate_now
    terminate_now = True
    logging.warning('User Interrupt ! Early Termination')
    save_file()
    sys.exit(0)

signal.signal(signal.SIGINT, exit_graceful)
#signal.signal(signal.SIGTERM, exit_graceful)


# seed random on host ID (for experimental reproducibility)
rd.seed(HOST)
data = array('f', list(rd.gauss(0, args.std_deviation) for i in range(0, LENGTH)))
logging.info('Seed: %s', str(data.tolist()))
logging.info('tx   rx  s#  r#  data')
receive_count = 0
sent_count = 0

class TransmitThread (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        time.sleep(1 + rd.uniform(0,0.9) )

    def run (self):
        logging.warning('Start main thread')
        
        # Initialize a UDP socket for sending
        cs = socket(AF_INET, SOCK_DGRAM)
        cs.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        cs.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
        global terminate_now
        global sent_count

        for sent_count in range(0, BROADCAST_COUNT-1):

            # wait constant interval between sending
            time.sleep(1)
            msg = bytes([HOST]) + data.tobytes() 
            # logging.info("%3.3i   ? %3.3i %3.3i %s", HOST, sent_count, 
            #    receive_count, str(data.tolist()))
            logging.debug('SENT: ' + str(HOST) + ' ' + str(data))
            logging.log(DISGUSTING, 'SENT HEX: %s', str(msg))
            cs.sendto(msg, (ADDR, PORT))
            if terminate_now:
                break

        cs.close()

tx_t = TransmitThread()
tx_t.start()

# Initialize UDP socket for listening (same address)
cs = socket(AF_INET, SOCK_DGRAM)
try:
    cs.bind((ADDR, PORT))
except:
    logging.critical('Failed to bind')
    cs.close()
    raise
    cs.blocking(0)

logging.warning('Start receiving thread')

for receive_count in range(0, (BROADCAST_COUNT - 2) * args.nodes_up ):

    # Uses set interval -- needs to "get lucky" to avoid collisions
    # time.sleep(1) 
    rx_data = cs.recvfrom(2 + 4 * LENGTH)  
    data_dec = rx_data[0]

    # parses the data 
    peer = int.from_bytes(data_dec[0:1], byteorder='big')
    extern_avg = data_dec[1:]
    new_data = array('f', extern_avg)
    
    logging.info('%3.3i %3.3i %3.3i %3.3i %s', peer, HOST, 
        sent_count, receive_count, str(new_data.tolist()))
    #logging.debug('Received from Host ID: %i %s', peer, str(rx_data[1]))
    #logging.debug('Payload: %s', str(new_data))
    history.append(new_data.tolist())

    for i in range(0, LENGTH):
        data[i] = (data[i] + new_data[i]) / 2

    logging.log(DISGUSTING, 'New Avg: %s', repr(data))

    
tx_t.join()
logging.warning('Most Packets Successfully received! Terminating safely...')
save_file()
cs.close() 
