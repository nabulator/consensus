import numpy as np
from sklearn.feature_extraction.image import extract_patches_2d

def extractImg(img,featureVecSize,NumSamples,rng):
    data = extract_patches_2d(img, featureVecSize, max_patches=NumSamples, random_state=rng)
    data = np.reshape(data, (len(data),-1))
    data = data.T
    return data, np.shape(data)


