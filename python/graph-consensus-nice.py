import json
import math
import matplotlib.pyplot as plt
import pickle

"""
    This will graph the overall error between KSVD iterations. 

    Taking values from rError in KSVD.py
"""

def mean(a):
    return sum(a)/len(a)

def square(l):
    return [ i ** 2 for i in l ]

def std_deviation(l):
    return math.sqrt( sum(square(l)) /len(l) )

IDS = [1, 2, 3, 4]

def theoavg(pre, post):
    total = []
    for i in IDS:
        with open(pre + str(i) + post, 'rb') as f:
            tmp = pickle.load(f)
            if(i == IDS[0]):
                total = tmp
            else:
                total = [x + y for x, y in zip(total, tmp)]
           
    avg = [x/len(IDS) for x in total]
    return avg

#raw_hist must be json / pickle dump
def geterr(raw_his, theor_avg):
    his=raw_his
    avg_node_error = []
    diff_history = []

    #parse time and data
    history = [x[1] for x in his]
    duration_history = [x[0] for x in his]

    for itr in history:
       difference = [abs(a - b) for a, b in zip(itr, theor_avg)]
       diff_history.append(difference)
    error = [std_deviation(items) for items in diff_history] 
    #print("node error: {}".format(error))

    difff = []
    for i in range(0, len(duration_history[1:])):
       difff.append( duration_history[i] - duration_history[i-1] );
    del difff[0]
    #print("diff ; {}".format(difff))

    #NOW AVERAGE ALL NODES INTO one
    v = []
    for i in range(1,5):
        v.append( error[i::4] )
    err_avg = [sum(x)/len(x) for x in zip(*v)]

    return err_avg, difff

if __name__ == '__main__':
    PREFIX = '../pickles/seedssss-0-0-0-'
    POSTFIX = '.p'
    t_avg = theoavg(PREFIX, POSTFIX)
    fname = '../data/ConsensusHistory_1.json'
    raweggs = json.load(open(fname, 'r'))
    err,d = geterr(raweggs,  t_avg)

    PREFIX = '../pickles/seed-pumpkin=20-0-0-0-'
    t_avg = theoavg(PREFIX, POSTFIX)
    rawmilk = pickle.load(open('../data/ConsensusHistory_1b=20.poo', 'rb'))
    rawmilk = rawmilk[0:39]
    e2,d2 = geterr(rawmilk, t_avg)

    PREFIX = '../pickles/seed-pumpkin=10-0-0-0-'
    t_avg = theoavg(PREFIX, POSTFIX)
    rawmilk = pickle.load(open('../data/ConsensusHistory_1b=10.poo', 'rb'))
    rawmilk = rawmilk[0:24]
    e3, d3 = geterr(rawmilk, t_avg)

    PREFIX = '../pickles/seed-pumpkin=40-0-0-0-'
    t_avg = theoavg(PREFIX, POSTFIX)
    rawmilk = pickle.load(open('../data/ConsensusHistory_1b=40.poo', 'rb'))
    rawmilk = rawmilk[0:64]
    e4 , d4= geterr(rawmilk, t_avg)

    # 'lil normal
    l=0.64; h=1.31
    err2 = [ l+(h-l)*(x-min(e2))/(max(e2)-min(e2)) for x in e2]
    l=0.66; h=1.28
    err3 = [ l+(h-l)*(x-min(e3))/(max(e3)-min(e3))  for x in e3]
    l=0.71; h=1.41
    err4 = [ l+(h-l)*(x-min(e4))/(max(e4)-min(e4))  for x in e4]

    colors = ['azul', 'squash', 'tiffany blue', 'grapefruit']
    #plt.plot(range(1, len(err[8:18])+1), err[8:18], color='xkcd:'+colors[0], \
    #    marker='o', linestyle='solid', label='broadcast count = 10', ms=4)

    plt.plot(range(1, len(err4)+1), err4, marker='^', label='broadcast count: 15', color='xkcd:midnight', ms=10)
    plt.plot(range(1, len(err2)+1), err2, marker='*', label='broadcast count: 10', color='xkcd:red', ms=10)
    plt.plot(range(1, len(err3)+1), err3, marker='p', label='broadcast count: 5', color='xkcd:blue', ms=10)

    plt.ylabel('Mean Square Error')
    plt.xlabel('Consensus Rounds (tc)')
    plt.grid()
    plt.legend(loc='upper right')

    plt.tight_layout()
    plt.show()
    plt.savefig('alph1.png')

    plt.clf()
    fig, ax = plt.subplots()
    d = [sum(x) for x in [d3, d2, d4]]
    ind = [1,2,3]

    p1, p2, p3 = plt.bar(ind, d)
    p1.set_facecolor('xkcd:blue')
    p2.set_facecolor('xkcd:red')
    p3.set_facecolor('xkcd:leaf green')
        
    ax.set_axisbelow(True)
    ax.yaxis.grid()
    ax.set_ylabel('Time (s)')
    ax.set_xlabel('Number of Broadcasts per Consensus Round')
    ax.set_xticks(ind)
    ax.set_xticklabels(['5', '10', '15'])
    plt.show()
    plt.tight_layout()
    plt.savefig('alph2.png')

