import pickle
import numpy as np

ID = [1,2,3,4]

for i in ID:
    np.random.seed(i)
   
    mu, sigma = 0, 0.5
    Y = np.random.normal(mu, sigma, (64, 100))
    D = np.random.normal(mu, sigma, (64, 50))

    #concat
    if i is 1:
        D_concat = D
        Y_concat = Y
    else:
        D_concat = np.concatenate((D_concat, D), axis=1)
        Y_concat = np.concatenate((Y_concat, Y), axis=1)

print("D\n" + str(D_concat))
print("Y\n" + str(Y_concat))

pickle.dump(D_concat, open('d-concat.p', 'wb'))
pickle.dump(Y_concat, open('y-concat.p', 'wb'))

