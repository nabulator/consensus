from timeit import default_timer as timer
import pickle
import numpy as np
#from sklearn import preprocessing

def OMP(D,Y,L):
    N = D.shape[0]
    K = D.shape[1]
    P = Y.shape[1]
    A = np.matrix('')

    # Ensure dimension of dictionary is same as that of signals
    if(N != Y.shape[0]):
        print("Feature-size does not match!")
        return

    for k in range(0,P):
        a = []
        x = Y[:,k]
        residual = x
        indx = [0]*L

        for j in range(0,L):
            proj = np.dot(np.transpose(D),residual)
            k_hat = np.argmax(np.absolute(proj))
            indx[j] = k_hat
            t1 = D[:,indx[0:j+1]]
            a = np.dot(np.linalg.pinv(t1),x)
            residual = x - np.dot(D[:,indx[0:j+1]],a)
            if(np.sum(np.square(residual)) < 1e-6):    #1e-6 = magic number to quit pursuit
                break
        temp = np.zeros((K,1))
        temp[indx[0:j+1]] = a

        if (A.size == 0):
            A = temp
        else:
            A = np.column_stack((A,temp))
    return A


#D = np.matrix( np.random.rand(4, 50) )
#Y = np.matrix( np.random.rand(4, 30) )
t0 = 3 #2
tD = 10
#Y = np.matrix([[1,2,3,4],[5,6,7,8]])
#D = np.matrix([[1,1,1,1],[1,1,1,1]])*0.7071
Y = np.matrix(pickle.load( open('y-concat.p', 'rb') ) )
D = np.matrix(pickle.load( open('d-concat.p', 'rb') ) )
N = D.shape[0]
K = D.shape[1]
print(D)
print(Y)
#D_scaled = preprocessing.scale(D) -- this is feature scaling not l2 normalization (which is what we want)
#print(D_scaled)
#D = preprocessing.normalize(D, axis=0)
rerror = np.zeros(tD)
x_history = []

start = timer()
for t in range(0, tD):
    print('Outer Iteration number = %d' % t)
    x = OMP(D,Y,t0)
    x_history.append(x)
    for k in range(0, K):
        print('Inner Iteration number = %d' %k)
        wk = [i for i,a in enumerate((np.array(x[k,:])).ravel()) if a!=0]
        Ek = (Y-np.dot(D,x)) + (D[:,k]*x[k,:])
        ERk = Ek[:,wk]

        if len(wk)!= 0:
            U1, S1, V1 = np.linalg.svd(ERk)
            D[:,k] = (U1[:,0])
            x[k,wk] = (V1.T[:,0]*S1[0]).reshape(V1.shape[0],)
    rerror[t] = np.linalg.norm(Y-np.dot(D,x))

end = timer()
print('Centralized dictionary learning takes %f [s]' % (end - start))
print(rerror)
print(x_history)
pickle.dump(rerror, open('centralized_err.p', 'wb'))
pickle.dump(x_history, open('centralized_x_history.p', 'wb'))

