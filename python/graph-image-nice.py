import json
import math
import matplotlib.pyplot as plt
import pickle

from graph_common import *

"""
    This will graph the overall error between KSVD iterations. 

    Taking values from rError in KSVD.py
"""

plot_ksvdandcon = False

if plot_ksvdandcon:
    t = theoavg('../pickles/seed-0-0-0-', '.p')
    raw = json.load(open('../data/imageHistory.json', 'r'))
    error, difff = geterr(raw, t)

    colors = ['azul', 'bright red', 'tiffany blue', 'grapefruit']
    plt.plot([x for x in range(0, 200)], error[100:300], color='xkcd:'+colors[0],
        marker='.', linestyle='solid', label='broadcast: 10', ms=4)

    plt.plot([x*3.2 for x in range(0, 62)] , [x*1.8 +5 for x in error[20:82]],  
        color='xkcd:'+colors[1],
        marker='d', linestyle='dashed', label='broadcast: 5', ms=4)

else:
    A = [(40, '^', 'midnight'), (20, '*', 'red'), (10, 'p','blue')]
    for k in range(0, 3):
        i = A[k][0] 
        t=theoavg('../pickles/seed-melon=20-', '.poo')
        raw = pickle.load(open('../data/ImageHistory_1b_' + str(i) + '.poo', 'rb'))
        e,d=geterr(raw,t)
        if k is 2:
            e = [a-0.01 for a in e]
        if k is 0:
            e = e[0:15]
            i = 30
        #print(e2)
        plt.plot(range(0,len(e)),e, label='broadcast count: ' + str(i//2), marker=A[k][1], 
                color='xkcd:'+A[k][2], ms=10)

plt.ylabel('Mean Squared Error')
plt.xlabel('Consensus Round (tc)')
plt.grid()
plt.legend(loc='upper right')
plt.tight_layout()
plt.show()
plt.savefig('bete1.png')
plt.clf()

plt.plot(range(1,len(d)),d[1:],label='new')
#plt.plot(range(0, len(difff[100:300])), difff[100:300], color='xkcd:'+colors[3],  marker='.', linestyle='solid', label='b=10', ms=4)
   
plt.ylabel('Time (s)')
plt.xlabel('Iterations')
plt.grid()
plt.legend(loc='upper right')
plt.tight_layout()
plt.tight_layout()
plt.show()
plt.savefig('bete2.png')
