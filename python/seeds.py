from random import seed, gauss

def final_vals(ids, length, std):
    ''' Calculates final values for experiment

        Since seeds are based on id's and id's are fixed at the begining of each
        experiment. We can effectively calculate the exact theoretical average
        of any given experiment. Note that even when seeded, random.gauss(mu,
        sigma) produces different output for the pait of inputs.
    '''
    fin = [0] * length
    for i in ids:
        seed(i)
        for j in range(0, length):
            fin[j] = fin[j] + gauss(0, std)
            # print('id: ', i, fin[j])
    
    for j in range(0, length):
        fin[j] = fin[j] / len(ids)

    with open( str(std) +'.seed', 'w') as file:
        file.write( repr(fin) )

    return fin 
