import json
import math
import matplotlib.pyplot as plt
from seeds import final_vals as ff

"""
    Graphs the mean squared error within a single round of consensus.
    Takes JSON file (log) of the history array as spat out by the global traffic. 
    Takes multiple standard deviation files. 

    Compares experimental data with seeds (precalcalced from seeds package)

    Notes: 
    Uses rr.py to create "experimental" data. Highly controlled.
"""

def mean(a):
    return sum(a) / len(a)

def square( l ):
    return [ i ** 2 for i in l ]

def std_deviation( l ):
    return math.sqrt( sum(square(l)) / len(l) )

IDS = [1, 2, 3, 4]
LENGTH = 64
STD_DEVS = [0.3, 0.5, 0.7, 0.9]

PREFIX = '../history_1_'
POSTFIX = '.json'

avg_node_error = []

for std in STD_DEVS:
    with open(PREFIX + str(std) + POSTFIX, 'r') as f:
        history =  json.load(f)
        diff_history = []

        theor_avg = ff(IDS, LENGTH, std)
        for itr in history:
           difference = [abs(a - b) for a, b in zip(itr, theor_avg)]
           diff_history.append(difference)

        node_error = []
        for i in range( 0, len(IDS) ):
            node_error.append( [std_deviation(items) for items in diff_history[i::len(IDS)]] )
            
        # Average the individual node error
        avg_node_error.append( list(map(mean, zip(*node_error))) )

        print("STD: {} \nDATA: {}".format(std, avg_node_error))

colors = ['mango', 'carolina blue', 'kermit green', 'tomato red']
for i in range(0, len(STD_DEVS)):
    plt.plot(range(0, len(avg_node_error[i])), avg_node_error[i], color='xkcd:'+colors[i], \
        marker='.', linestyle='None', label='sig=' + str(STD_DEVS[i]), ms=((15-i)))

plt.ylabel('sqrt( error^2 )')
plt.xlabel('rx iterations')
plt.grid()
plt.legend(loc='upper right')
plt.title('\sigma(error) over time')
plt.show()
plt.savefig('lrr1.png')
