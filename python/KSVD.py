import pickle
import os
import sys
import time
import argparse
import json
from matplotlib import image
from timeit import default_timer as timer

import numpy as np
from numpy import linalg as LA

from ExtractRandomPatches import *
from cloud import *

# Maybe in the future...
# import scipy as sp
# import mnist
# from PIL import Image

def CloudKSVD(D,Y,refvec,tD,t0,tc,tp,weights,comm,c,node_id,broadcast_count,nodes_up,globalSyncOffset):
    rank = 0
    size = 0
    ddim = np.shape(D)[0]
    K = np.shape(D)[1]
    S = np.shape(Y)[1]
    x = np.matrix(np.zeros((K,S)))
    rerror = np.zeros(tD)
    time_OMP = np.zeros(tD)
    time_comm = np.zeros(tD)
    d_hist = []

    #tD iterations of kSVD
    for t in range(0, tD):  
        if rank == 0:
            print('=================Iteration %d=================' % (t+1))

        #[STEP:3 SPARSE CODING
        start = timer()
        for s in range(0, S): 
             x[:,s] = OMP(D,Y[:,s],t0) #t0 is the sparsity constraint
        end = timer()
        time_OMP[t] = (end-start)
        print('OMP one loop time %f' % time_OMP[t])

        start = timer()
        #[STEP: 4 DICTIONARY UPDATE]
        for k in range(0, K):
            #if rank == 0:
            #    print('Updating atom %d' % (k+1))


            #Error matrix
            wk = [i for i,a in enumerate((np.array(x[k,:])).ravel()) if a!=0]
            Ek = (Y-np.dot(D,x)) + (D[:,k]*x[k,:])
            ERk = Ek[:,wk]

            #Power Method
            if np.size(wk) == 0: #if empty
                M = np.matrix(np.zeros((ddim,ddim)))
            else:
                M = ERk*ERk.transpose()
            #pickle.dump(M, open('M-' + str(t) + '-' + str(k) + '-'  + str(node_id) + '.p', 'wb'))    
            q = powerMethod(M,tc,tp,weights,comm,c,node_id,broadcast_count,nodes_up,globalSyncOffset, k, t)
            #pickle.dump(q, open('q-' + str(t) + '-'  + str(k) + '-' + str(node_id) + '.p', 'wb'))
            
            #Codebook Update
            if np.size(wk) != 0: #if not empty
                refdirection = np.sign(np.array(q*refvec)[0][0])
                if LA.norm(q) != 0:
                    D[:,k] = (refdirection*(q/(LA.norm(q)))).reshape(ddim,1)
                else:
                    D[:,k] = q.reshape(ddim,1)
                x[k,:] = 0
                x[k,wk]= np.array(D[:,k].transpose()*ERk).ravel()

        end = timer()
        time_comm[t] = (end - start)

        #Error Data
        rerror[t] = np.linalg.norm(Y-np.dot(D,x))
        d_hist.append(D)
        #print("Node %s Iteration %d error:" % (node_id,t+1) , rerror[t])
        #time.sleep(0.2)

    return D,x,rerror, d_hist, time_OMP, time_comm

def OMP(D,Y,L):
    N = D.shape[0]
    K = D.shape[1]
    P = Y.shape[1]
    A = np.matrix('')

    # Ensure dimension of dictionary is same as that of signals
    if(N != Y.shape[0]):
        print("Feature-size does not match!")
        return

    for k in range(0,P):
        a = []
        x = Y[:,k]
        residual = x
        indx = [0]*L

        for j in range(0,L):
            proj = np.dot(np.transpose(D),residual)
            k_hat = np.argmax(np.absolute(proj))
            indx[j] = k_hat
            t1 = D[:,indx[0:j+1]]
            a = np.dot(np.linalg.pinv(t1),x)
            residual = x - np.dot(D[:,indx[0:j+1]],a)
            if(np.sum(np.square(residual)) < 1e-6):    #1e-6 = magic number to quit pursuit
                break
        temp = np.zeros((K,1))
        temp[indx[0:j+1]] = a

        if (A.size == 0):
            A = temp
        else:
            A = np.column_stack((A,temp))
    return A

def powerMethod(M,tc,tp,weights,comm_worldObject,graphObject,node_id, broadcast_count,nodes_up,globalSyncOffset,
        k, t ): #debuggin variables
    datadim = M.shape[0]
    rank = 0
    size = 0

    q = np.matrix(np.ones((datadim,1)))
    qnew = q
    phi = np.matrix(np.zeros((datadim,size)))
    CorrCount = 1

    for powerIteration in range(0,tp,1): # each iteration of the power method
        #print("~~~~~~~~~~~~ powerItr {0}/{1} ~~~~~~~~~~~~~~~~~".format(powerIteration,tp))


        #pickle.dump(M, open('M_before-' + str(t) + '-' + str(k) + '-'+  str(powerIteration)   + '-' + str(node_id) + '.p', 'wb'))
        #pickle.dump(qnew, open('qnew_before-' + str(t) + '-' + str(k) + '-'+  str(powerIteration)   + '-' + str(node_id) + '.p', 'wb'))

        qnew = (M*qnew) # We use corrective consensus here, regular consensus works too
        
        #[Replace with Distributed consensus]

        flattenedMatrix = list(i for i in qnew.T.flat )

        # helps grab intial seeds for graphing...
        #if( t == 0 and k == 0 and powerIteration == 0 ):
        #    pickle.dump(flattenedMatrix, open('seed-melon=' + str(broadcast_count) +'-'+ str(t) + '-' + str(k) + '-'+  str(powerIteration)   + '-' + str(node_id) + '.poo', 'wb'))

        # Consensus happens

        cloud = ConsensualNetworking(flattenedMatrix, node_id, broadcast_count, nodes_up, globalSyncOffset, powerIteration)
        cloud.performConsensus()
        #time.sleep(0.2) # try to better sync  between different machines
        newdata = cloud.close()

        qnew = np.matrix( [ newdata ] ).T
        #pickle.dump(qnew, open('qnew_consensus-' + str(t) + '-' + str(k) + '-'+  str(powerIteration)  + '-' + str(node_id) + '.p', 'wb'))
        
        if LA.norm(qnew) != 0:
            qnew /= LA.norm(qnew) # normalize
        #pickle.dump(qnew, open('qnew_consensus_norm-' + str(t)  + '-' +  str(k) + '-'+ str(powerIteration) + '-'  + str(node_id) + '.p', 'wb'))    

    eigenvector = qnew.reshape(datadim)
    return eigenvector



def main():

    ## dirty argparse
    parser = argparse.ArgumentParser(description='Cloud K_SVD Node', epilog='Parul Pandy & Brian Qiu 2018')
    parser.add_argument('-i', dest='host_id', help='Manually Assigns ID to node', type=int, required=True)
    parser.add_argument('-b', dest='broadcast_count', type=int, default=20, 
        help='Broadcast packets per round of consensus')
    parser.add_argument('-p', dest='power_iteration_count', type=int, default=1, 
        help='Rounds of consensus per power method call')
    parser.add_argument('-N', dest='nodes_up', type=int, default=1,
        help='Number of total nodes up and running. Used to help estimate when\
        to terminate program. Default: 1') 
    parser.add_argument('-s', dest='globalSyncOffset', type=int, default=-1,
        help='Synchronizes the beginning of the all nodes. Requires syncronized clocks. Performs initial \
            sleep on all the nodes. Default: Disabled')
    parser.add_argument('--text', dest='text', default="",
        help="text to append to some pickle files.")

    args = parser.parse_args()

    np.random.seed(args.host_id)

    ### QUICK & DIRTY TEST CODE
    # refvec = np.random.rand(2,1)
    # D = np.matrix([[0.3162, 0.4472, 0.6644, 1], [0.9487, 0.8944, 0.7474, 0]])
    # Y = np.matrix([[1, 2], [3, 4]])

    ### BENCHMARKING TEST CODE
    #refvec = np.random.rand(4, 1)
    #D = np.matrix( np.random.rand(4, 2) )
    #Y = np.matrix( np.random.rand(4, 4) )

    featureVecSize = (8, 8)
    NumSamples = 64
    rng = 0;#np.random.RandomState(0)
    Y_orig = image.imread('001.jpg')  
    Y_orig = Y_orig[:,:,1]
    Y, Y_S= extractImg(Y_orig,featureVecSize,NumSamples,rng)
    Y = np.matrix(Y)/255 
    D = np.matrix(np.random.random((64, 32)))

    #mu, sigma = 0, 0.5
    #Y = np.matrix( np.random.normal(mu, sigma, (64, 100)))
    #D = np.matrix( np.random.normal(mu, sigma, (64, 50)))
    #print("Y: {} {}".format(Y, Y.shape))
    #print("D: {} {}".format(D, D.shape))

    ddim = np.shape(D)[0]
    refvec = np.matrix(np.ones((ddim,1)))

    tD=10
    t0=3 #sparsity constraint
    tc=1
    tp= 2 #args.power_iteration_count
    weights = 0
    c = 0
    comm = 0

    # node attributes (passed to cloud.py)
    node_id = args.host_id
    broadcast_count = args.broadcast_count
    nodes_up = args.nodes_up 
    globalSyncOffset = args.globalSyncOffset

    start = timer()
    D,X,rerror,d_hist,time_OMP,time_COMM = CloudKSVD(D,Y,refvec,tD,t0,tc,tp,weights,comm,
            c,node_id,broadcast_count,nodes_up,globalSyncOffset)
    end =  timer()
    total_time = (end-start)
    
    
    pickle.dump(np.dot(D,X), open('reconstructed_' + str(node_id) + '.p', 'wb'))
    pickle.dump(time_OMP, open('time_omp' + str(node_id) + '.p', 'wb'))
    pickle.dump(time_COMM, open('time_comm' + str(node_id) + '.p', 'wb'))
    #pickle.dump(rerror, open('dist_err-' + str(node_id) + '.p', 'wb'))
    #pickle.dump(D, open('dist_final_D-' + str(node_id) + '.p', 'wb'))
    #pickle.dump(X, open('dist_final_X-' + str(node_id) + '.p', 'wb'))
    #pickle.dump(d_hist, open('dist_hist_X-' + str(node_id) + '.p', 'wb'))
    #f = open('IterationError' + str(node_id) + '.json', 'w')
    #f.write( json.dumps(rerror.tolist()))
    #f.close()
    pickle.dump(rerror, open('error-' + str(node_id) + args.text + '.p', 'wb'))
    pickle.dump(Y, open('Y-' + str(node_id) + '.p', 'wb'))

    print("THE ERRROR IS {}".format(rerror))
    print("THE TOTAL COMPUTATION TIME IS {}".format(np.sum(time_OMP)))
    print("THE TOTAL COMMUNICATION TIME IS {}".format(np.sum(time_COMM)))
    print("THE TOTAL EXECUTION TIME IS {}".format(np.sum(total_time)))

# if __name__ == "main":
main()

