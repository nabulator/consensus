import json
import math
import copy
import matplotlib.pyplot as plt
from seeds import final_vals as ff

"""
    This will graph the simulation of missing nodes. It behaves the same as 
    graph.py 

    Also uses rr.py for the creation of experimental data.
"""

def mean(a):
    return sum(a) / len(a)

def square( l ):
    return [ i ** 2 for i in l ]

def std_deviation( l ):
    return math.sqrt( sum(square(l)) / len(l) )

IDS = [1, 2, 3, 4]
LIVE_IDS = copy.deepcopy(IDS)
LIVE_IDS.remove(3)
LENGTH = 64
STD_DEVS = [0.3, 0.5, 0.7, 0.9]

PREFIX = '../history_1_'
POSTFIX = '.json'

avg_node_error = []

for std in STD_DEVS:
    with open(PREFIX + str(std) + POSTFIX, 'r') as f:
        history =  json.load(f)
        diff_history = []

        theor_avg = ff(IDS, LENGTH, std)
        for itr in history:
           difference = [abs(a - b) for a, b in zip(itr, theor_avg)]
           diff_history.append(difference)

        node_error = []
        for i in range( 0, len(LIVE_IDS) ):
            node_error.append( [std_deviation(items) for items in diff_history[i::len(LIVE_IDS)]] )
            
        # Average the individual node error
        avg_node_error.append( list(map(mean, zip(*node_error))) )

        print("STD: {} \nDATA: {}".format(std, avg_node_error))

colors = ['azul', 'squash', 'tiffany blue', 'grapefruit']
for i in range(0, len(STD_DEVS)):
    plt.plot(range(0, len(avg_node_error[i])), avg_node_error[i], color='xkcd:'+colors[i], \
        marker='.', linestyle='None', label='sig=' + str(STD_DEVS[i]), ms=((15-i)))

plt.ylabel('sqrt( error^2 )')
plt.xlabel('rx iterations')
plt.grid()
plt.legend(loc='upper right')
plt.title('\sigma(error) over time WITH DEAD NODE')
plt.show()
plt.savefig('deadrr1.png')
