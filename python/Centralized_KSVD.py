from timeit import default_timer as timer
import pickle
import numpy as np
#from sklearn import preprocessing

def OMP(D,Y,L):
    N = D.shape[0]
    K = D.shape[1]
    P = Y.shape[1]
    A = np.matrix('')

    # Ensure dimension of dictionary is same as that of signals
    if(N != Y.shape[0]):
        print("Feature-size does not match!")
        return

    for k in range(0,P):
        a = []
        x = Y[:,k]
        residual = x
        indx = [0]*L

        for j in range(0,L):
            proj = np.dot(np.transpose(D),residual)
            k_hat = np.argmax(np.absolute(proj))
            indx[j] = k_hat
            t1 = D[:,indx[0:j+1]]
            a = np.dot(np.linalg.pinv(t1),x)
            residual = x - np.dot(D[:,indx[0:j+1]],a)
            if(np.sum(np.square(residual)) < 1e-6):    #1e-6 = magic number to quit pursuit
                break
        temp = np.zeros((K,1))
        temp[indx[0:j+1]] = a

        if (A.size == 0):
            A = temp
        else:
            A = np.column_stack((A,temp))
    return A


      
#Y = np.matrix(pickle.load( open('C:\\Users\\Parul\Desktop\\CloudKSVD_results\\old\\y-concat.p', 'rb') ) )
#D = np.matrix(pickle.load( open('C:\\Users\\Parul\Desktop\\CloudKSVD_results\\old\\d-concat.p', 'rb') ) )

t0 = 3
tD = 1
mu, sigma = 0, 0.1
D = np.matrix( np.random.normal(mu, sigma, (64, 100)))
Y = np.matrix( np.random.normal(mu, sigma, (64, 400)))
N = D.shape[0]
K = D.shape[1]
rerror = np.zeros(tD)
x_history = []

for t in range(0, tD):
#    print('Outer Iteration number = %d' % t)
    start = timer()
    x = OMP(D,Y,t0)
    end = timer()

    x_history.append(x)
    for k in range(0, K):
#        print('Inner Iteration number = %d' %k)
        wk = [i for i,a in enumerate((np.array(x[k,:])).ravel()) if a!=0]
        Ek = (Y-np.dot(D,x)) + (D[:,k]*x[k,:])
        ERk = Ek[:,wk]

        if len(wk)!= 0:
            U1, S1, V1 = np.linalg.svd(ERk)
            D[:,k] = (U1[:,0])
#            print(x[k,wk].shape)
#            print((V1.T[:,0]*S1[0]).reshape(V1.shape[0],).shape)
            x[k,wk] = (V1.T[:,0]*S1[0]).reshape(V1.shape[0],)
    rerror[t] = np.linalg.norm(Y-np.dot(D,x))/np.sqrt(32*400)
    print(rerror[t])

centerror = np.linalg.norm(Y-np.dot(D,x))
print("Centralized Error is %f" % centerror)
print("Time taken %f" % (end-start))

