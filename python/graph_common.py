import json
import math
import matplotlib.pyplot as plt
import pickle

"""
    This will graph the overall error between KSVD iterations. 

    Taking values from rError in KSVD.py
"""

def mean(a):
    return sum(a)/len(a)

def square(l):
    return [ i ** 2 for i in l ]

def std_deviation(l):
    return math.sqrt( sum(square(l)) /len(l) )

def avg4(a):
    return [ mean(j) for j in [ a[y:y+4] for y in range(0, len(a), 4)]]

def frange(x, y, jump):
    while x<y:
        yield x
        x+=jump

IDS = [1, 2, 3, 4]

def theoavg(pre, post):
    total = []
    for i in IDS:
        with open(pre + str(i) + post, 'rb') as f:
            tmp = pickle.load(f)
            if(i == IDS[0]):
                total = tmp
            else:
                total = [x + y for x, y in zip(total, tmp)]
           
    avg = [x/len(IDS) for x in total]
    return avg

#raw_hist must be json / pickle dump
def geterr(raw_his, theor_avg):
    his=raw_his
    avg_node_error = []
    diff_history = []

    #parse time and data
    history = [x[1] for x in his]
    duration_history = [x[0] for x in his]

    for itr in history:
       difference = [abs(a - b) for a, b in zip(itr, theor_avg)]
       diff_history.append(difference)
    error = [std_deviation(items) for items in diff_history] 
    #print("node error: {}".format(error))

    difff = []
    for i in range(0, len(duration_history[1:])):
       difff.append( duration_history[i] - duration_history[i-1] );
    #print("diff ; {}".format(difff))

    #NOW AVERAGE ALL NODES INTO one
    err_avg = avg4(error)

    return err_avg, difff

