import json
import math
import matplotlib.pyplot as plt
import pickle
import heapq
import numpy as np

"""
    This will graph the overall error between KSVD iterations. 
    BUT THIS one uses final nice values?

    Taking values from rError in KSVD.py
"""

IDS = [1, 2, 3, 4]
colors = ['azul', 'squash', 'bright orange', 'grapefruit', 'true blue']

# graph rerror
PREFIX = '../pickles/error-'
POSTFIX = '.p'
total_hist = []

for std in [1, 2, 4]:
    with open(PREFIX + str(std) + POSTFIX, 'rb') as f:
        history = pickle.load(f)
        total_hist.append(history)


#second trial
PREFIX = '../pickles/error-'
POSTFIX = '00.p'

rr = range(0,10)
for i in [1, 2, 3, 4]:
    for j in ['00']:
        with open(PREFIX + str(i) + j + '.p', 'rb') as f:
            history = pickle.load(f)
            total_hist.append(history)
            #plt.plot(rr, history, linestyle='solid', linewidth=1, label='2' +str(i), ms=6)

for i in total_hist:
        print("r_error: {}".format(i))

aaa = [sum(x)/len(x) for x in zip(*total_hist)]
maxa = [heapq.nlargest(2, x)[1] for x in zip(*total_hist)] #grab second largest
mina = [heapq.nsmallest(2,x)[1] for x in zip(*total_hist)] #grab second smallest
diffb = [(a-b) for a,b in zip(maxa, aaa)]
diffc = [(a-b) for a,b in zip(aaa, mina)]


print('maxs')
print(maxa)

#plt.errorbar(rr, aaa, [diffc, diffb], color='xkcd:'+colors[2], \
#    marker='x', linestyle='solid', linewidth=1, elinewidth=0.1, label='Avg', ms=6)

plt.plot(rr, maxa, color='xkcd:'+colors[3], \
    marker='>', linestyle='solid', linewidth=1, label='90%', ms=6)
plt.plot(rr, mina, color='xkcd:'+colors[0], \
    marker='<', linestyle='solid', linewidth=1, label='10%', ms=6)

plt.plot(rr, aaa, color='xkcd:'+colors[2], \
    marker='x', linestyle='solid', linewidth=1, label='Avg', ms=6)



#for i in range(0, len(IDS)):
#    plt.plot(range(0, len(total_hist[0])), total_hist[i], color='xkcd:'+colors[i], \
#        marker='.', linestyle='solid', linewidth=1, label='node='+str(i), ms=4)

plt.ylabel('Mean Squared Error')
plt.xlabel('K-SVD Iteration (tD)')
plt.grid()
plt.legend(loc='upper right')
plt.tight_layout()
plt.show()
plt.savefig('realbigksvd.png')

total_hist = []
PREFIX = '../pickles/time_omp'
plt.clf()
fig, ax1 = plt.subplots() #grab axis
ax2 = ax1.twinx()


# Now graph comp/comm time
if False:

    for std in IDS:
        with open(PREFIX + str(std) + POSTFIX, 'rb') as f:
            history = pickle.load(f)
            if(len(total_hist) == 0 ):
                total_hist = [0] * len(history)
            total_hist = total_hist + history

    total_hist = [x/4 for x in total_hist]

    p1, =  ax1.plot(range(0, len(total_hist)), total_hist, color='xkcd:'+colors[2], \
        marker='d', linestyle='solid', label='OMP', ms=8)

    total_hist = []
    ind = []
    PREFIX = '../pickles/time_comm'
    for std in IDS:
        with open(PREFIX + str(std) + POSTFIX, 'rb') as f:
            history = pickle.load(f)
            ind.append(history)
            #print(history)
            if(len(total_hist) == 0 ):
                total_hist = [0] * len(history)
            total_hist = total_hist + history

    total_hist = [x/4 for x in total_hist]

    p2, = ax2.plot(range(0, len(total_hist)), total_hist, color='xkcd:'+colors[4], \
        marker='h', linestyle='solid', label='Commnication', ms=12)


    ax1.set_ylabel('OMP Time (s)')#, color='xkcd:'+colors[2])
    ax2.set_ylabel('Comm Time (s)')#(, color='xkcd:'+colors[4])
    ax1.set_xlabel('K-SVD Iterations')
    ax1.grid()
    ax2.grid()
    num_tick=6 #manual setting of ticks belwo
    ax1.set_yticks(np.linspace(0, ax1.get_ybound()[1]+1, num_tick))
    ax2.set_yticks(np.linspace(0, ax2.get_ybound()[1]+1, num_tick))
    plt.legend(handles=[p1,p2],loc='lower right')
    plt.tight_layout()
    plt.show()
    plt.savefig('comptime.png')
