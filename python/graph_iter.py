import json
import math
import matplotlib.pyplot as plt
import pickle

"""
    This will graph the overall error between KSVD iterations. 

    Taking values from rError in KSVD.py
"""

IDS = [1, 2, 3, 4]
LENGTH = 64

PREFIX = '../IterationError'
POSTFIX = '.json'
total_hist = []

for std in IDS:
    with open(PREFIX + str(std) + POSTFIX, 'r') as f:
        history = json.load(f)
        total_hist.append(history)

colors = ['azul', 'squash', 'tiffany blue', 'grapefruit']
for i in range(0, len(IDS)):
    plt.plot(range(0, len(total_hist[0])), total_hist[i], color='xkcd:'+colors[i], \
        marker='.', linestyle='None', label='node='+str(i), ms=4)

# comparison to centralized ksvd
rerr_cent = pickle.load(open('centralized_err.p', 'rb'))
plt.plot(range(0, len(rerr_cent)), rerr_cent, color='xkcd:pinkish red', \
    marker='o', linestyle='None', label='centralized', ms=12)

plt.ylabel('sqrt( error^2 )')
plt.xlabel('KSCD iterations')
plt.grid()
plt.legend(loc='upper right')
plt.title('\sigma(error) over ITERATIONS')
plt.show()
plt.savefig('iterErr2.png')
