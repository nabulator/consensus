from socket import *
from array import *
import time
import datetime
import threading
import logging
import sys
import signal
import pickle
import random as rd

ADDR = '192.168.1.255'
PORT = 4490
DISGUSTING = 5
COLORS = ['\033[' + base + 'm' for base in ['0;33', '1;34', '1;35', '1;36', '0;37']]
COLOR_CLEAR = '\033[;0m'

def applyColor(string, x):
    return COLORS[ x % len(COLORS)] + string + COLOR_CLEAR

# Global variables shared between sender and receiver 
terminate_now = False
performed_sync = False
data = array('f', [])

class ConsensualNetworking:

    """
    The main logic behind a working consensual node.
    Holds the starting state for a node just as hostname, initial data. Sets up
    logging and signal handlers. Includes logic for receiving and parsing data.

    init_data   -- expects an list at the moment...
    host_id     -- the node id 
    

    """
    # stores history of individual rounds of consensus data
    history = []

    def __init__(self, init_data, host_id=1, broadcast_count=20, nodes_up=1, clock_start_at_second=-1, roundNumber=1):
    
        if broadcast_count > 255:
            raise ValueError('Broadcast count must be less than 256')

        # setup sig handler
        signal.signal(signal.SIGINT, self.exit_graceful)
        #signal.signal(signal.SIGTERM, exit_graceful)

        # restricts host range to one byte
        if host_id < 0 or host_id > 255:
            self.HOST = host_id % 256
        else:
            self.HOST = host_id

        if not isinstance(init_data, list):
            raise TypeError('data must be type list')

        global data
        data = array('f', init_data)
        self.broadcast_count = broadcast_count
        self.nodes_up = nodes_up

        # setup logging
        logging.addLevelName(5, 'DISGUSTING')
        FORMAT = '%(asctime)s [%(levelname)-4.4s] ' \
            + applyColor('{' + str(self.HOST) + "}", host_id)  +  " %(message)s"
        logging.basicConfig(format=FORMAT, level=logging.INFO)

        log = logging.getLogger()
        self.handler = logging.FileHandler('formated_' + str(self.HOST) + '.log')
        self.handler.setLevel(logging.INFO)
        self.handler.setFormatter( logging.Formatter('%(asctime)s %(message)s') )
        log.addHandler(self.handler)

        # seed random on host ID (for experimental reproducibility)
        rd.seed(self.HOST)
        
        
        logging.info("~~~ Consensus Round: %i", roundNumber)

        #attempts global sync based on clock
        global performed_sync

        # -- negative clock_start means no sync
        if performed_sync is False and not clock_start_at_second < 0:
            t = datetime.datetime.today()
            future = datetime.datetime(t.year, t.month, t.day, t.hour, t.minute, clock_start_at_second)
            if t.second > clock_start_at_second:
                future += datetime.timedelta(minutes=1)
            diff = future - t
            secs_to_sleep = diff.total_seconds()
            logging.info('sleeping this time %f us: %d', secs_to_sleep, diff.microseconds)
            time.sleep(secs_to_sleep)
            performed_sync = True

        #logging.info('Seed: %s', str(data.tolist()))
        logging.debug('tx   t#  rx  r#  data')

    def exit_graceful(self, signum, frame):
            print('ute Puppies')
            global terminate_now
            terminate_now = True
            logging.warning('User Interrupt ! Early Termination')
            self.save_file()
            sys.exit(0)

    def save_file(self):
        f = open('ImageHistory' + '_' + str(self.HOST) + 'b_' + str(self.broadcast_count) + '.poo', 'ab')
        pickle.dump(self.history, f)
        f.close()


    def performConsensus(self):

        logging.debug('Start Rx Thread')

        # Initialize UDP socket for listening (same address)
        self.cs = socket(AF_INET, SOCK_DGRAM)
        self.cs.settimeout(5)
        try:
            self.cs.bind((ADDR, PORT))
        except:
            logging.critical('Failed to bind')
            self.cs.close()
            raise
            self.cs.blocking(0)

        # Start transmitting thread
        tx_t = TransmitThread(self.HOST, self.broadcast_count)
        tx_t.start()

        # Packet failure threshold 50 %
        receive_count = 0
        for receive_count in range(0, (self.broadcast_count // 2) * self.nodes_up + 1 ):

            try:
                rx_data = self.cs.recvfrom(2 + 4 * len(data))  
            except OSError as err:
                logging.error('Waiting too long... probably. ===> {0}'.format(err))
                break

            data_dec = rx_data[0]

            # parses the data 
            peer = int.from_bytes(data_dec[0:1], byteorder='big')
            sent_count = int.from_bytes(data_dec[1:2], byteorder='big')
            extern_avg = data_dec[2:]
            new_data = array('f', extern_avg)
            
            #logging.debug( applyColor('%3.1i ', peer) + '%3.2i ' + applyColor('%3.1i ', self.HOST) \
            #    + '%3.2i %s', peer, sent_count, self.HOST,  \
            #    receive_count, str(new_data.tolist()))
            #logging.debug('Received from Host ID: %i %s', peer, str(rx_data[1]))
            #logging.debug('Payload: %s', str(new_data))
            self.history.append( [time.time(), new_data.tolist()] )

            for i in range(0, len(data)):
                data[i] = (data[i] + new_data[i]) / 2

            #logging.log(DISGUSTING, 'New Avg: %s', repr(data))

            
        received = receive_count + 1

        #Check if above threshold
        if received > (self.broadcast_count * self.nodes_up) // 2:
             logging.info('MOST Packets received! [%d/%d]',\
                received, self.broadcast_count * self.nodes_up)
        else:
            logging.warning('missed threshold. some Packets received: \
            [%d/%d] Joining Tx thread ...', received, self.broadcast_count * self.nodes_up)

        #TODO use Events to terminate thread early!
        # > Nah, this works rather well as of now.
        terminate_now = True
        tx_t.join()

    
    ''' 
        cleans up logging , saves file
        also return final values for consensus
        REQUIRED because of multithreading
    '''
    def close(self):
        #self.save_file()
        log = logging.getLogger()
        log.removeHandler( self.handler )
        self.cs.close() 
        return data

class TransmitThread (threading.Thread):

    """
    Corresponding Transmit Thread which transmits the data at regular intervals.
    Works without knowledge of the happenings in the receive thread.

    """

    def __init__(self, host, broadcast_count):
        self.HOST = host
        self.broadcast_count = broadcast_count
        super(TransmitThread, self).__init__()

    def run(self):
        logging.debug('Start Tx thread')
        
        # Initialize a UDP socket for sending
        cs = socket(AF_INET, SOCK_DGRAM)
        cs.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        cs.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
        global terminate_now
        global data
    
        for sent_count in range(0, self.broadcast_count - 1):

            # wait constant interval between sending
            time.sleep(0.08)
            msg = bytes([self.HOST]) + bytes([sent_count]) + data.tobytes() 
            # logging.info("%3.3i   ? %3.3i %3.3i %s", HOST, sent_count, 
            #    receive_count, str(data.tolist()))
            #logging.log(DISGUSTING, 'SENT: ' + str(self.HOST) + ' ' + str(data))
            #logging.log(DISGUSTING, 'SENT HEX: %s', str(msg))
            cs.sendto(msg, (ADDR, PORT))
            if terminate_now:
                break

        cs.close()



def main():
   
    a = ConsenualNetworking()
    a.run()
    a.close()

