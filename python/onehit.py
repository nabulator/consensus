import pickle
import os
import sys
import time
import argparse
import json
from matplotlib import image
from timeit import default_timer as timer

import numpy as np
from numpy import linalg as LA

from ExtractRandomPatches import *
from cloud import *

       
def main():

    ## dirty argparse
    parser = argparse.ArgumentParser(description='Cloud K_SVD Node', epilog='Parul Pandy & Brian Qiu 2018')
    parser.add_argument('-i', dest='host_id', help='Manually Assigns ID to node', type=int, required=True)
    parser.add_argument('-b', dest='broadcast_count', type=int, default=20, 
        help='Broadcast packets per round of consensus')
    parser.add_argument('-p', dest='power_iteration_count', type=int, default=1, 
        help='Rounds of consensus per power method call')
    parser.add_argument('-N', dest='nodes_up', type=int, default=1,
        help='Number of total nodes up and running. Used to help estimate when\
        to terminate program. Default: 1') 
    parser.add_argument('-s', dest='globalSyncOffset', type=int, default=-1,
        help='Synchronizes the beginning of the all nodes. Requires syncronized clocks. Performs initial \
            sleep on all the nodes. Default: Disabled')
    args = parser.parse_args()

    np.random.seed(args.host_id)

    ### QUICK & DIRTY TEST CODE
    # refvec = np.random.rand(2,1)
    # D = np.matrix([[0.3162, 0.4472, 0.6644, 1], [0.9487, 0.8944, 0.7474, 0]])
    # Y = np.matrix([[1, 2], [3, 4]])

    ### BENCHMARKING TEST CODE
    #refvec = np.random.rand(4, 1)
    #D = np.matrix( np.random.rand(4, 2) )
    #Y = np.matrix( np.random.rand(4, 4) )

    featureVecSize = (8, 8)
    NumSamples = 64
    rng = 0;#np.random.RandomState(0)
    Y_orig = image.imread('001.jpg')  
    Y_orig = Y_orig[:,:,1]
    Y, Y_S= extractImg(Y_orig,featureVecSize,NumSamples,rng)
    Y = np.matrix(Y)/255 
    D = np.matrix(np.random.random((64, 32)))

    # node attributes (passed to cloud.py)
    node_id = args.host_id
    broadcast_count = args.broadcast_count
    nodes_up = args.nodes_up 
    globalSyncOffset = args.globalSyncOffset

    print(Y)
    flattenedMatrix = list(Y.flat)
    

    pickle.dump(flattenedMatrix, open('seed-melon=' + str(broadcast_count) + '-' + str(node_id) + '.poo', 'wb'))

    # Consensus happens

    cloud = ConsensualNetworking(flattenedMatrix, node_id, broadcast_count, nodes_up, globalSyncOffset, 1)
    cloud.performConsensus()
    cloud.close()

main()

