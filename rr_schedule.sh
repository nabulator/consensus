#!/bin/bash

# scheduler.sh 
# @author Brian Qiu
# @time Summer 2018 
#
# Starts python scripts at around the same time
#
# Usage: 
# 	-s <float> Floating point for standard deviation. Passed to python scripts.
#	-n <int>   Optional node to "shutdown" to simulate node that is down.
#

# Some Colors
NC='\e[0m'
RED='\033[31m'

usage() { echo "Usage: $0 [-s <float>] [-n <int>]" 1>&2; exit 1; }


hosts=(pinus red magenta)
hostcount=${#hosts[@]}

while getopts ":s:n:" o; do
	case "${o}" in 
		s)
			s=${OPTARG}
			;;
		n)
			n=${OPTARG}
			(( n >= 1 || n <= ${hostcount} )) || usage
			;;
		*)
			usage
			;;
	esac
done
shift $((OPTIND-1))

if [ -z "${s}" ]; then
	usage
fi


if [ -n "${n}" ]; then 
	hostcount=$(( ${hostcount} - 1 ))
fi

for i in "${!hosts[@]}"; do
	if [ $(($i + 1)) -eq ${n} ]; then
		echo "skip ${i}"
		continue
	fi
	(
		ssh "${hosts[i]}" /bin/bash <<- EOSSH

			fancy_echo()
			{
				echo -e "$(tput setaf $((i + 3)))	\$1 $NC"
			}	

			# NODE STATUS
			fancy_echo "Node: \t$i"
			fancy_echo "Host: \t${hosts[i]}"
			fancy_echo "IP: \t\$(hostname -i)"
			fancy_echo "Time: \t\$(date +"%T.%N")" 

			# SCHEDULE TIME
			# echo "$(date +"%T.%N")" 

			if [ -f rr.py ];
				# Node indexes must start from 1
				then 
				set -x
				python3 rr.py -i "$(( 1 + $i ))" -n 64 -s ${s} -N ${hostcount}
				set +x
				fancy_echo "${hosts[i]} done"
			else
				echo -e "\033[31m"
				echo "[ERROR] rr.py not found on ${hosts[i]}. exitting"
				echo -e "\e[0m"
				exit 1
			fi

		EOSSH
	) &
done
wait
echo All subshells finished
