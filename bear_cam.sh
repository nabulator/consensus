#!/bin/bash
# rkll.sh
# -------------------------
# Let's kill all python processes

hosts=(pi@192.168.2.2 pi@192.168.2.8)
files=(cloud.py KSVD.py)

cd python
for i in "${!hosts[@]}"; do 
	set -x
		ssh "${hosts[i]}" /bin/bash <<- EOSSH
			cd Pictures
			raspistill -o "{$hosts[i]}1.jpg"
			EOSSH
	{ set +x; } 2>/dev/null
done
