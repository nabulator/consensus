#!/bin/bash
# Update.sh
# -------------------------
# Simple script to copy files to their hosts!

hosts=(green cyan white magenta)
files=(cloud.py KSVD.py) #Centralized_KSVD.py ExtractRandomPatches.py 001.jpg)

cd python
for i in "${!hosts[@]}"; do 
	for j in "${!files[@]}"; do
		set -x
		scp -q "${files[j]}" "${hosts[i]}":"${files[j]}"
		{ set +x; } 2>/dev/null
	done
done
