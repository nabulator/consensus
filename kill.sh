#!/bin/bash
# rkll.sh
# -------------------------
# Let's kill all python processes

hosts=(green cyan white magenta)
files=(cloud.py KSVD.py)

cd python
for i in "${!hosts[@]}"; do 
	set -x
		ssh "${hosts[i]}" /bin/bash <<- EOSSH
			pkill -f raspivid
			pkill -f python
			# pkill -INT -f python
			EOSSH
	{ set +x; } 2>/dev/null
done
