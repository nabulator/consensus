#!/bin/bash

# scheduler.sh 
# @author Brian Qiu
# @time Summer/Fall/Winter 2018 
#
# Starts python scripts at around the same time
#
# Usage: 
# 	-b <int>   Broadcast count per "consensus step". Passed to python scripts.
# 	-p <int>   Consensus round count per "PowerMethod()". Passed to python scripts.
#	-n <int>   Optional node to "shutdown" to simulate node that is down.
#	-t 	   Test. Just print node information
#	-v	   Verbose. Have nodes output consensus with verbiage!

# Some Colors
NC='\e[0m'
RED='\033[31m'

usage() { echo "Usage: $0 [-tv] [-b <int>] [-p <int>] [-n <int>] " 1>&2; exit 1; }


hosts=(green white cyan magenta)
hostcount=${#hosts[@]}
b=20
p=2

while getopts ":b:p:n:z:tv" o; do
	case "${o}" in 
		b)
			b=${OPTARG}
			;;
		n)
			n=${OPTARG}
			(( n >= 1 || n <= ${hostcount} )) || usage
			;;
		p)
			p=${OPTARG}
			;;
		t)
			t=1
			;;
		v)
			v=1 
			echo "set verbose"
			;;
		z)
			z=${OPTARG}
			echo "woah $z"
			;;
		*)

			usage
			;;
	esac
done
shift $((OPTIND-1))

CUR_SECS="$(date +%S)"
NODE_START_AT_SECONDS="$(( ($CUR_SECS + 6) % 60))"

if [ -n "${n}" ]; then 
	hostcount=$(( ${hostcount} - 1 ))
fi

#trap 'echo "LOL"' SIGINT

for i in "${!hosts[@]}"; do
	if [[ $(($i + 1)) -eq "${n}" ]]; then
		echo "skip ${i}"
		continue
	fi
	(
		ssh "${hosts[i]}" /bin/bash <<- EOSSH

			fancy_echo()
			{
				echo -e "$(tput setaf $((i + 3)))	\$1 $NC"
			}	

			# NODE STATUS
			fancy_echo "Node: \t$i"
			fancy_echo "Host: \t${hosts[i]}"
			fancy_echo "IP: \t\$(hostname --all-ip-addresses)"
			fancy_echo "Time: \t\$(date +"%T.%N")" 

			trap 'echo "woah, Im in here"' SIGINT

			# SCHEDULE TIME
			# echo "$(date +"%T.%N")" 
			if [[ -n "$t" ]];
				# Quit if TEST flag is enabled
				then
				exit 0
			elif [[ -f cloud.py && -f KSVD.py ]];
				# Node indexes must start from 1
				then 
				set -x
				python3 KSVD.py -i "$(( 1 + $i ))" -b "$b" -p "$p" -N ${hostcount} --text "$z"
				#python3 onehit.py -i "$(( 1 + $i ))" -b "$b" -p "$p" -N ${hostcount} 
        # -s "$NODE_START_AT_SECONDS"
        #python3 Centralized_KSVD.py 
				{ set +x; } 2>/dev/null
				fancy_echo "${hosts[i]} done"
			else
				echo -e "\033[31m"
				echo "[ERROR] cloud.py not found on ${hosts[i]}. exitting"
				echo -e "\e[0m"
				exit 1
			fi

		EOSSH
	) &
done
wait
echo All subshells finished
