# consensus

A framework for achieving consensus within an asynchronous network. 
Ideally a standin for HD security cameras with high bandwidth requirements.
Compression obtained from overlapping images of the same object reduces bandwidth
and makes for smaller offline files.

## Requirements
Python 3.2 and above for individual node experiment.

### Scheduler.sh 

Not required to run network but highly recommended to avoid _manual_ labor.
Bash and getopts are required for node manager. Has only been tested on Linux.
Current Nodes run on Raspberry Pi versions 2 and 3 with ssh daemons.
Maybe I'll port this script to python in the near future. :)

### Graphing 
For purposes of plotting, matplotlib is required. :(

### Cloud Script 
Numpy is required as is its linear algebra modules. In the Future, PIL (pillow)
and scipy may be required for image processing. 

Optionally, Colorama if you want ANSI coloring on windoze.

## Usage

 > I need help running running the basic round robin node! :-O 

` python3 rr.py --help`

 > I'd rather run a script to coordinate multiple nodes! >:O

` ./rr_schedule.sh ` 

  Note you may have to change the hostnames in the script and configure
  your nodes to ssh without prompt.

 > I want to run the actual K-SVD algorithm with matrices!

 `python3 cloud.rr --help`

 > Do you have a scheduler for that too?
 >> Yes! I'm glad you asked...
 
 `./cloud_scheduler.sh`

## Plotting Data

 > I need to plot my data! How ? :-o

 ` python3 graph.py`

 > Now I want to plot the graphs with one node down! How ?
 
 ` python3 graph2.py`


## License

  We'll figure out what goes here later...

Copyright (c) 2018 Summer

